# KomputerStore

## Description
The solution shows a webpage that gives the users to do work by clicking the work button. The pay given can be used to store in the bank or repay a loan. 
A user can take a loan from the bank at max the double of what exsist in the bank. A user can only recive on loan at a time, and must repay the loan before taking another.
The user can use the earned money to buy a laptop from a select menu. The money is taken from the useres bank account.

## Hosting
The solution is hosted on: https://sirtakin.gitlab.io/komputerstore/

## Author
This solution was created by Kristian Garberg.