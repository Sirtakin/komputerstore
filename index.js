const bankBalanceElemtent = document.getElementById("bank-balance")
const loanElement = document.getElementById("loan")
const computersElement = document.getElementById("computers")
const featuresElement = document.getElementById("features")
const featuresListElement = document.getElementById("list-of-features")
const payBalanceElement = document.getElementById("pay-balance")
const computerCardElement = document.getElementById("computer-card-section")

//Buttons
const loanButtonElement = document.getElementById("loan-button")
const bankButtonElement = document.getElementById("bank-button")
const workButtonElement = document.getElementById("work-button")
const repayButtonElement = document.getElementById("repay-button")
const buyButtonElement = document.getElementById("buy-now-button")

//API base-URL
const baseUrl = "https://noroff-komputer-store-api.herokuapp.com/"

let computers = []
let bankBalance = 0
let bankLoan = 0
let payBalance = 0
let bankPayback = 0
let computerPrice = 0;
const workPay = 100

//Get endpoint
fetch(baseUrl + "computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputers(computers))
    .catch(function (error) {
        console.log('Error:', error)        
    })

const addComputers = (computers) => {
    computers.forEach(x => addComputer(x))
}

const addComputer = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}

//Event listeners
computersElement.addEventListener('change', function () {
    featuresListElement.innerText = ""
    let filteredData = null
    let selectedId = computersElement.options.selectedIndex

    filteredData = computers.filter((i, index) => {
        return index === selectedId
    })

    let { title, description, specs, price, image } = filteredData[0]
    let imageUrl = baseUrl + image
    computerPrice = price

    console.log(image)

    for (const spec of specs) {
        createSpecsList(spec)
    }

    computerCardElement.innerHTML = createComputerCard(imageUrl, title, description, price)
})

workButtonElement.addEventListener('click', function () {
    payBalance += workPay
    payBalanceElement.innerText = payBalance + ` kr`
    return payBalanceElement
})

bankButtonElement.addEventListener('click', function () {
    if (!bankLoan) {
        bankBalance += payBalance
        payBalance = 0
        bankBalanceElemtent.innerText = bankBalance + ` kr`
        payBalanceElement.innerText = payBalance + ` kr`
    } else if (bankLoan > 0) {
        bankLoan -= (payBalance * 0.1)
        payBalance = payBalance * 0.9
        bankBalance += payBalance
        payBalance = 0

        if (bankLoan <0) 
            bankLoan = 0        

        loanElement.innerText = bankLoan
        bankBalanceElemtent.innerText = bankBalance + ` kr`
        payBalanceElement.innerText = payBalance + ` kr`
    } else if ( payBalance * 0.1 > bankLoan) {
        payBalance = bankLoan - payBalance
        payBalance = payBalance * (-1)
        bankBalance += payBalance
        bankLoan = 0
        payBalance = 0
        loanElement.innerText = bankLoan
        bankBalanceElemtent.innerText = bankBalance + ` kr`
        payBalanceElement.innerText = payBalance + ` kr`
    }
    return bankBalanceElemtent
})

loanButtonElement.addEventListener('click', function () {
    if (!bankLoan) {
        getLoan()
    } else {
        alert('You can\'t get a loan at this moment')
    }

    function getLoan() {
        let askForLoan = prompt('Enter the sum you want to laon. Max:' + bankBalance * 2)
        if (askForLoan > bankBalance * 2) {
            alert('You cant ask for this much. Keep it under ' + bankBalance * 2)
            getLoan()
        }
        bankLoan = Number(askForLoan)
        bankBalance += bankLoan
        bankBalanceElemtent.innerText = ""
        bankBalanceElemtent.innerText += bankBalance + ` kr`
        loanElement.innerText = bankLoan + ` kr`
        return loanElement
    }
})

repayButtonElement.addEventListener('click', function () {
    if (bankLoan > 0) {
        if (payBalance > bankLoan) {
            payBalance = bankLoan - payBalance
            bankLoan = 0
            payBalance = payBalance * (-1)
            console.log(payBalance)
            bankBalance += payBalance
            payBalance = 0
            loanElement.innerText = bankLoan
            bankBalanceElemtent.innerText = bankBalance + ` kr`
            payBalanceElement.innerText = payBalance + ` kr`
        } else if (payBalance) {
            let tempBank = bankLoan
            bankLoan = tempBank - payBalance
            payBalance = 0
            payBalanceElement.innerText = payBalance + ` kr`
            loanElement.innerText = bankLoan + ` kr`
        }
    } else {
        alert('No loan to pay back on')
    }
})

buyButtonElement.addEventListener('click', function () {
    buyPC(computerPrice)
})

function errorHandler () {
    alert("Image was not found (404)")
}


//Display info for computers
function createSpecsList(specs) {
    const featureElement = document.createElement('ul', { is: 'expanding-list' })
    featureElement.appendChild(document.createTextNode(specs))
    featuresListElement.appendChild(featureElement)
}

function createComputerCard(image, title, description, price) {
    return (
        `<div>
            <img src=${image} alt="No image found" onerror="errorHandler()">
            <h2>${title}</h2>
            <p>${description}</p>
            <p>${price}</p>
            <p>NOK</p>                                
        </div>`
    )
}

function buyPC(price) {
    if (bankBalance >= price && price != 0) {
        bankBalance -= price
        bankBalanceElemtent.innerText = ""
        bankBalanceElemtent.innerText += bankBalance
        alert("Congratulation on your new laptop!")
    } else {
        alert("Insufficient funds!")
    }
}